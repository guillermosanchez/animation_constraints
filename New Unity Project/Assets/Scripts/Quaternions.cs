﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quaternions : MonoBehaviour {

    public float w = 0.0f;
    public float x = 0.0f;
    public float y = 0.0f;
    public float z = 0.0f;

    // Use this for initialization
    void Start ()
    {

    }

    float Length()
    {
        return Mathf.Sqrt(Mathf.Pow(w, 2) + Mathf.Pow(x, 2) + Mathf.Pow(y, 2) + Mathf.Pow(z, 2));
    }

    public static Quaternions operator *(Quaternions a, Quaternions b)
    {
        Quaternions aux = new Quaternions();
        aux.w = a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z;
        aux.x = a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y;
        aux.y = a.w * b.y - a.x * b.z + a.y * b.w + a.z * b.x;
        aux.z = a.w * b.z + a.x * b.y - a.y * b.x + a.z * b.w;
        return aux;
    }
	// Update is called once per frame
	void Update () {

	}
}
