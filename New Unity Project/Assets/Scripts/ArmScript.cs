﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmScript : MonoBehaviour {

    public Transform joint0, joint1, joint2, joint3, joint4, zahando, target;
    private Quaternion qr;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update ()
    {
        qr.SetLookRotation(Vector3.Normalize(target.position - joint0.position), Vector3.up);
        joint0.rotation = Quaternion.Normalize(new Quaternion(0,qr.y,0, qr.w));
	}
}
