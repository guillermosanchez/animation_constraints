﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignmentScript : MonoBehaviour
{

    public Transform target1;
    public Transform target2;
	public int exercise = 1;
    Quaternion offsetTarget1;
    Quaternion offsetTarget2;


    // Use this for initialization
    void Start ()
    {
        offsetTarget1 = target1.rotation * Quaternion.Inverse(transform.rotation);
        offsetTarget2 = target2.rotation * Quaternion.Inverse(transform.rotation);
    }

    void Update()
    {
        switch(exercise)
        {
            
            case 1:
                {
                    Vector3 axisX = Vector3.Cross(transform.right, target1.right).normalized;
                    float angleX = -Mathf.Acos(Vector3.Dot(transform.right, target1.right)) * Mathf.Rad2Deg;

                    target1.Rotate(axisX, angleX, Space.World);

                    Vector3 axisY = Vector3.Cross(transform.up, target1.up).normalized;
                    float angleY = -Mathf.Acos(Vector3.Dot(transform.up, target1.up)) * Mathf.Rad2Deg;

                    target1.Rotate(axisY, angleY, Space.World);
                }
                break;

          

            case 2:
                {
                    target1.rotation = Quaternion.Lerp(target1.rotation, transform.rotation, Time.time * 0.05f);
                } break;

            case 3:
                {
                    target1.rotation = transform.rotation * offsetTarget1;
                    target2.rotation = transform.rotation * offsetTarget2;
                } break;

            case 4:
                {
                    target1.rotation = transform.rotation * offsetTarget1;
                    target2.rotation = target1.rotation * Quaternion.Inverse(offsetTarget1);
                } break;
        }
    }
}
