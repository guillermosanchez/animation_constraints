﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class planeConstraints : MonoBehaviour
{
    public bool active;
    public bool debugLines;

    public Transform parent;
    public Transform child;

    public Transform plane;

    private Vector3 vectorU;

    struct recta
    {
        public Vector3 point;
        public Vector3 vec;
    }


    // To define how "strict" we want to be
    private float threshold = 0.00001f;
 
    void Start()
    {
        vectorU = parent.position - child.position;
    }

    void LateUpdate()
    {
        Vector3 proj = Vector3.ProjectOnPlane(vectorU, plane.up);
        recta t;
        t.point = parent.position;
        t.vec=plane.up;
    }


    
    private float ComputeAngle(Vector3 ToParent, Vector3 ToChild)
    {
        return 0.0f;
    }

    


}
