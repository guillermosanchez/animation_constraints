﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class angleConstraints : MonoBehaviour
{
    public bool active;

    public mirrorMovement mirror;

    public Transform parent;
    public Transform child;

    [Range(0.0f, 180.0f)]
    public float minAngle;

    [Range(0.0f, 180.0f)]
    public float maxAngle;

    private Quaternion initialState;
    private Vector3 axis = new Vector3();
    

    void Start()
    {
        initialState = transform.localRotation;
    }

    void LateUpdate()
    {
        Quaternion angleOffset = (mirror.original.transform.localRotation * Quaternion.Inverse(initialState));
        float angle = Mathf.Acos(angleOffset.w) * 2;

        if(angle * Mathf.Rad2Deg > maxAngle)
        {
            Vector3 v1 = parent.transform.position - transform.position;
            Vector3 v2 = child.transform.position - transform.position;
            Vector3 eje = Vector3.Cross( v2.normalized, v1.normalized);
            eje.Normalize();
            Vector3 v = Mathf.Sin(maxAngle * Mathf.Deg2Rad / 2) * eje;
            Quaternion newRot = new Quaternion(v.x, v.y, v.z, Mathf.Cos(maxAngle * Mathf.Deg2Rad / 2));
            transform.localRotation = newRot;
        }

    }

    //add auxiliary functions, if needed, below




}
